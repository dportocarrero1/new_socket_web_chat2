<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DefaultMessage extends Model
{
    protected $table = 'default_messages';
}
