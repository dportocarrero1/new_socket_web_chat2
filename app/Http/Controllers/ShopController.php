<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Events\Refresh;
use App\Services\ChannelService;
use App\Services\ShopService;
use App\Services\UserService;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function login($country,Request $request)    {

        $request->validate([
            'code'    => 'required'
        ]);

        $dataShop = [
            'type'  => 'shop',
            'country'  => $country,
            'code'  => $request->code
        ];

        $response = ShopService::validateShop((object)$dataShop);



        if ($request->phoneConsumer) {
            $dataConsumer = [
                'user'  => $request->phoneConsumer,
                'type'  => 'consumer',
                'country'  => $country,
            ];
            UserService::infoUser((object)$dataConsumer);
            $data = [
                'consumer' =>  $request->phoneConsumer,
                'shop'     =>  $request->code,
                'country'  =>  $country,
            ];
            $response['channel'] = ChannelService::infoChannel((object)$data);
            $channels = Channel::where('shop',$request->code)->get();
            \Event(new Refresh(["channels" => $channels],$request->code));
        }
        return response()->json($response);
    }
}
