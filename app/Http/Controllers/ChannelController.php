<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Country;
use App\DefaultMessage;
use App\Events\Chat;
use App\Events\CountMessages;
use App\Events\Refresh;
use App\Http\Controllers\api\v1\ShowRequestController;
use App\Message;
use App\Services\ShopService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ChannelController extends Controller
{

    public function listChannel(Request $request)    {
        $channelInfo = Channel::where([
            [$request->user()->type, $request->user()->uid],
        ])->get();
        
        \Event(new Refresh(["channels" => $channelInfo],$request->user()->uid));
        return response()->json($channelInfo, Response::HTTP_OK);
    }

    public function message (Request $request) {
        $request->validate([
            'channel_id'    => 'required',
            'message'    => 'required',
        ]);

        $message = Message::create([
            'channel_id' => $request->channel_id,
            'message'    => $request->message,
            'sender'    => $request->user()->uid,
        ]);

        if ($message) {
            $channel = Channel::where('channel_id',$request->channel_id)->first();


            if ($request->user()->type == 'consumer') {
                $channel->unReadShopMessage = $channel->unReadShopMessage + 1;
                $channel->unReadConsumerMessage = 0;
                $uidUpdate = $channel->shop;
                $typeUpdate = 'shop';
            }else{
                $channel->unReadConsumerMessage = $channel->unReadConsumerMessage + 1;
                $channel->unReadShopMessage = 0;
                $uidUpdate = $channel->consumer;
                $typeUpdate = 'consumer';
            }
            $channel->lastMessage = $request->message;
            $channel->dateLastMessage = Carbon::now()->timestamp;
            $channel->active = 1;
            $channel->save();
            $messages = Message::where('channel_id' ,$request->channel_id)->orderByDesc('id')->get();
            $channels = Channel::where([
                [$typeUpdate , $uidUpdate],
            ])->get();

            \Event(new Chat(["messages" => $messages], $request->channel_id));
            \Event(new Refresh(["channels" => $channels],$uidUpdate));
            $idUser = User::where([
                ['uid',$uidUpdate],
                ['country',$request->country],
                ])->first();
            \Event(new CountMessages(["countMessages" =>$idUser->countUnreadMessage ],$idUser->id));
        }
        $status = Response::HTTP_OK;
        // want to raise ClearCache event

        $response = [
            'status' => 'success',
            'type' => 'mensaje enviado',
        ];
        return response()->json($response, $status);
    }

    public function chatList (Request $request) {

        $request->validate([
            'channel_id'    => 'required',
        ]);
        $channel = Channel::where('channel_id',$request->channel_id)->first();
        if ($channel) {
            $response = Message::where('channel_id', $request->channel_id)->get();
            $status = Response::HTTP_OK;
        }else{
            $response = [
                'message' => "no se encontro la tienda"
            ];
            $status = Response::HTTP_NOT_FOUND;
        }
        return response()->json($response, $status);

    }

    public function MessageDefault($country) {
        $response = [];
        $country = Country::where('code_country',$country)->get();
        foreach ($country as $key => $value) {
            $response[$key] = [
                'message' => $value->messages
            ];
        }

        return response()->json($response);
    }

    public function get($country)
    {

        $response = Country::where('code_country',$country)->first();
        if ( $response ) {
            // dd(json_decode($response->messages_default));
            $response->messages_default = json_decode($response->messages_default);
            $response->list_colors = json_decode($response->list_colors);
            $response->functions_avaliable = json_decode($response->functions_avaliable);
            return response()->json($response);
        }else{
            $response = [
                'message' => 'Codigo de pais invalido'
            ];
            return response()->json($response);
        }

    }

    public function close (Request $request) {
        $channelInfo = Channel::where('channel_id',$request->ChannelSid)->first();
        $channelInfo->active = 0;
        $channelInfo->save();
        return response()->json(['canal cerrado']);
    }

    public function ReadMessage (Request $request) {
        $channelInfo = Channel::where([
            [$request->user()->type, $request->user()->uid],
            ['channel_id', $request->sid],
        ])->first();

        if ($request->user()->type == 'consumer') {
            $channelInfo->unReadConsumerMessage =  0;
        }else{
            $channelInfo->unReadShopMessage =  0;
        }
        $channelInfo->save();
        return response()->json(['Mensaje leido'],Response::HTTP_OK);
    }

    public function infoStore($country, Request $request)    {
        $channelInfo = Channel::where([
            [$request->user()->type, $request->user()->uid],
            ['channel_id', $request->codeChannel],
        ])->first();
        $store = ShopService::infoShop($country,$channelInfo->shop);

        if($store && !empty($store)){
            $rating = $store->rating;
            $typeStore = $store->type ? ucfirst($store->type) : 'Tienda';
            $image = (string)$store->pictures_urls[0];
            $html = "<div class='c-storeInfo'>
            <img class='c-storeInfo__photo' src='$image'/>
            <div class='c-storeInfo__content'>
                <div class='c-storeInfo__inline qualification'>
                    <p>$typeStore</p>
                    <p>
                        <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAACWSURBVHgBbU6xDcJADPQZKiqaQAsbpKYBNoAJYBNmyAZhAhiBgjBHWjrqKM7xb1D0oFiyZZ/vfBZJoq2yY8wUG6cDoKdvW/ZYU2W5iuRQrIVwdUcpAN46skZ7n5VheZCBYIczXPGYXyiy+7ETuerqudc4mFnxrzaDY04Adfq5yVeotbdqi57AETbxMZ00SwW2FAZvOOENYbQyKe0LCeAAAAAASUVORK5CYII='/>
                        <span>
                            <b>$rating->value</b>
                            ($rating->total)
                        </span>
                    </p>
                </div>
                <h2 class='c-storeInfo__name'>$store->title</h2>
                <div class='c-storeInfo__inline'>
                    <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAADBSURBVHgBTVCxEcIwDIxNBggdLRPEZIN0tEwQp6QCKkq8AWSCmBGYICPgEcIGmcDm/86+WI2k1+v1tigQWutKSnkJIegihhDCeu8Ha+0iSQAwAdiCdBjHcY/cohfEOS+pAOCDDRNVn6hvKA0JyNcynmizM6pYw6D/SlbYnLNBlQr6YS6T8QRA+dT3/YQ8oHXEJJo3fD2y7Rnmed4B7zjfKKXINk3T7Oq6/jnnlvgldxCOmJ1FOsdXwGSXFKmA9KKNPzNcZuhHpyjUAAAAAElFTkSuQmCC'/>
                    <p><b>Lun-Vie</b> $store->start_hour  - $store->end_hour </p>
                </div>
                <div class='c-storeInfo__inline'>
                    <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAJCAYAAAAGuM1UAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAACaSURBVHgBjY/BDcMgDEWNmwE6At2AbtBu0BEipuitkyC6QVbIsUdG6AhkAES/K6g4NAlPMhjZxv8rAtbaKed8o3Vm7/1VksMIcN9pG22MWUIIL1ZKPaiD2sfUT5RjgHbRNopOWY04lvyCeCNCqT+/A3UdM3vn3Nz8KI0Ei7rWfwNCSumM4umPFN0+hsbURB1wMRN3+iIULJJ8AAtlLAYqFv3RAAAAAElFTkSuQmCC'/>
                    <p>$store->address</p>
                </div>
            </div>
        </div>";
            $response = [
                'codeChannel' => $request->codeChannel,
                'html' => $html
            ];
        }else{
            $response = [
                'message' => 'no se encontro información de la tienda'
            ];
        }
        return response()->json($response);
    }

    public function messagesInfo($country, Request $request) {
        $response = [];
        $country = DefaultMessage::where('country',$request->codeCountry)->get();
        foreach ($country as $key => $value) {
            $response[$key] = [
                'message' => $value->messages
            ];
        }

        return response()->json($response);
    }
}
