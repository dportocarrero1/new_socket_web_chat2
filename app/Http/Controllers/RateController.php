<?php

namespace App\Http\Controllers;

use App\OptionRate;
use App\Rate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RateController extends Controller
{
    public function getOption(Request $request)
    {
        if(is_numeric($request->rateCount) && $request->rateCount <= 5 ){
            $typeRate = $request->rateCount >= 3 ? 'goodRate' : 'badRate';
            $response =  OptionRate::where('typeRate',$typeRate)->get();
            $codeResponse = Response::HTTP_OK;
        }else{
            $codeResponse = Response::HTTP_NOT_FOUND;
            $response = [
                'message' => 'error numero de calificación invalido'
            ];
        }
        return response()->json($response,$codeResponse);
    }

    public function create(Request $request)
    {
        $data = $request->toArray();
        Rate::create($data);
        $response = [
            'message' => 'Calificación exitosa'
        ];
        return response()->json($response,Response::HTTP_OK);
    }

    public function get()
    {
        $response = [
            ['text' => 'Mal servicio'],
            ['text' => 'Servicio regular'],
            ['text' => 'Servicio aceptable'],
            ['text' => 'Buen servicio'],
            ['text' => 'Servicio excelente'],
        ];

        return response()->json($response,Response::HTTP_OK);
    }
}
