<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Events\Refresh;
use App\Events\CountMessages;
use App\Services\ChannelService;
use App\Services\ShopService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login($country,Request $request)    {

        $request->validate([
            'phoneConsumer'    => 'required'
        ]);

        $dataConsumer = [
          'user'  => $request->phoneConsumer,
          'type'  => 'consumer',
          'country'  => $country,
          'name'  => $request->name
        ];

        $response = UserService::infoUser((object)$dataConsumer);

        if ($request->code && ShopService::infoShop($country,$request->code)) {

            $data = [
                'consumer' =>  $request->phoneConsumer,
                'shop'     =>  $request->code,
                'country'  =>  $country,
            ];
            $response['channel'] = ChannelService::infoChannel((object)$data);

        }
        $channels = Channel::where('consumer',$request->phoneConsumer)->get();
        \Event(new Refresh(["channels" => $channels],$request->phoneConsumer));
        \Event(new CountMessages(["countMessages" => $response['unreadMessage']],$response['id']));
        return response()->json($response);
    }
}
    