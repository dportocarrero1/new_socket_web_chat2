<?php

namespace App\Services;


use App\Shop;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ShopService extends Service{

    function __constructor(){

        parent::__constructor();
    }

    public static function infoShop($country,$code) {


        $url = config('services.storeInfo')[$country];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url/api/v2/shopkeeper/info/$code",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $info = curl_exec($curl);
        $codeResponse = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        Log::info("url info Store $url");
        Log::info('Response infoStore : '.$codeResponse);
        return json_decode($info);
    }

    public static function validateShop ($data) {

        $validateShop = User::where([
            ['uid' , $data->code],
            ['country' , $data->country],
            ['type' , $data->type],
        ])->first();

        if ($validateShop) {
            $infoShop = $validateShop;
        }else{
            $dataInfo = self::infoShop($data->country,$data->code);
            if($dataInfo){
                $infoShop = User::create([
                    'name' => isset($dataInfo->title) ? $dataInfo->title : 'shop_'.$data->code,
                    'uid' => $data->code,
                    'phone' => isset($dataInfo->phone) ? $dataInfo->phone : "123456709_$data->code",
                    'country' => $data->country,
                    'type' => $data->type,
                    'password' => bcrypt($data->code.$data->country)
                ]);
            }else{
                return false;
            }

        }


        Auth::loginUsingId($infoShop->id);
        $user = Auth::user();

        $tokenResult = $user->createToken('Personal Shop Access Token');

        $token = $tokenResult->token;
        return [
            'status' => 'success',
            'name' => $infoShop->name,
            'uid' => strval($infoShop->uid),
            'tokenWebsocket' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'local' => true,
            'type' => 'consumer',
        ];

    }
}
