<?php

namespace App\Services;


use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserService extends Service{

    function __constructor(){

        parent::__constructor();
    }
    public static function infoUser ($data) {
        $validateUser = User::where([
           ['uid' , $data->user],
            ['country' , $data->country],
            ['type' , $data->type],
        ])->first();

        if ($validateUser) {
            $infoUser = $validateUser;
        }else{
            $infoUser = User::create([
                'name' => isset($data->name) ? $data->name : 'consumidor_'.$data->user,
                'phone' => $data->user,
                'uid' => $data->user,
                'country' => $data->country,
                'type' => $data->type,
                'password' => bcrypt($data->user.$data->country)
            ]);
        }
        Auth::loginUsingId($infoUser->id);
        $user = Auth::user();

        $tokenResult = $user->createToken('Personal Consumer Access Token');
        $token = $tokenResult->token;
        return [
            'status' => 'success',
            'name' => $infoUser->name,
            'uid' => strval($infoUser->uid),
            'tokenWebsocket' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'local' => true,
            'type' => 'consumer',
            'id' => $user->id,
            'unreadMessage' => $user->countUnreadMessage
        ];
    }
}
