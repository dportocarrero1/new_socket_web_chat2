<?php

namespace App\Services;


use App\Channel;
use App\Shop;
use Illuminate\Support\Facades\Log;

class ChannelService extends Service{

    function __constructor(){

        parent::__constructor();
    }
    public static function infoChannel ($data) {
        $channel = Channel::where([
            ['country' , $data->country],
            ['consumer' , $data->consumer],
            ['shop'     , $data->shop],
            ['active'   , 1]
        ])->first();

        if($channel){
            $response = $channel;
        }else{
            $response = Channel::create([
                'country' => $data->country,
                'consumer' => $data->consumer,
                'shop' => $data->shop,
                'active' => 1,
                'channel_id' => 'CH'.md5($data->country . $data->consumer . $data->shop),
            ]);
        }

        return $response;
    }
}
