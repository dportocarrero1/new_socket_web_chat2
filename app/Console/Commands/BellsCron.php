<?php

namespace App\Console\Commands;

use App\User;
use App\Channel;
use App\Events\Refresh;
use App\Events\CountMessages;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BellsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Bells:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user) {
            $channels = Channel::where([
                [$user->type , $user->uid],
            ])->get();
            \Event(new CountMessages(["countMessages" =>$user->countUnreadMessage ],$user->id));
            \Event(new Refresh(["channels" => $channels],$user->uid));
        }
        Log::info('Cron success');
        return 'Cron success';
    }
}
