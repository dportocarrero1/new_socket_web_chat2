<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['channel_id','message','sender'];

    protected $hidden = ['channel_id','message','sender','created_at' , 'updated_at'];

    protected $appends = ['author', 'body' , 'date'];

    public function  getAuthorAttribute() {
        return $this->sender;
    }

    public function  getBodyAttribute() {
        return $this->message;
    }

    public function  getDateAttribute() {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->timestamp;
    }
}
