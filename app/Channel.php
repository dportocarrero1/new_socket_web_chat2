<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = [
        'channel_id', 'country', 'consumer','shop','active','unReadConsumerMessage','unReadShopMessage','dateLastMessage','lastMessage'
    ];

    protected $appends = [
        'channelSid','closed','codeStore','consumerName','consumerUnreadMessages','grocer','grocerName','grocerUnreadMessages', 'messages'
    ];

    protected $hidden = [
        'channel_id', 'country','active','unReadConsumerMessage','unReadShopMessage','created_at' , 'updated_at'
    ];

    public function getMessagesAttribute (){
        return Message::where([
            ['channel_id',$this->channel_id],
        ])->get();
    }

    public function getChannelSidAttribute (){
        return $this->channel_id;
    }

    public function getClosedAttribute (){
        return $this->active = 1 ? false : true;
    }

    public function getCodeStoreAttribute (){
        return $this->shop;
    }

    public function getConsumerNameAttribute (){
        $consumerInfo = User::where('uid',$this->consumer)->first();
        return $consumerInfo->name;
    }

    public function getConsumerUnreadMessagesAttribute (){
        return $this->unReadConsumerMessage;
    }

    public function getGrocerAttribute (){
        $grocerInfo = User::where('uid',$this->shop)->first();
        return isset($grocerInfo->phone) ? $grocerInfo->phone : 0 ;
    }
    public function getGrocerNameAttribute (){
        $grocerInfo = User::where('uid',$this->shop)->first();
        return isset($grocerInfo->name) ? $grocerInfo->name: null ;
    }
    public function getGrocerUnreadMessagesAttribute (){
        return $this->unReadShopMessage;
    }
}
