<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::group([
        'prefix' => 'V1'
    ], function () {
        Route::post('consumer-login', 'UserController@login');
        Route::post('grocer-login', 'ShopController@login');

        Route::group([
            'middleware' => 'auth:api'
        ], function() {

            Route::post('messages', 'ChannelController@message');
            Route::post('validateReadMessages', 'ChannelController@ReadMessage');
            Route::get('messageDefault', 'ChannelController@MessageDefault');
            Route::get('config', 'ChannelController@get');
            Route::get('messageDefault', 'ChannelController@messagesInfo');
            Route::get('channels', 'ChannelController@listChannel');

            Route::post('/close-channel', 'ChannelController@close');

            Route::get('rate/info', 'RateController@get');
            Route::post('rate/create', 'RateController@create');
            Route::get('rate/option/{rateCount}', 'RateController@getOption');

            Route::post('chats', 'ChannelController@chatList');
            Route::post('info-store', 'ChannelController@infoStore');

            Route::post('validateUser', 'AuthController@validateUser');

        });
    });
