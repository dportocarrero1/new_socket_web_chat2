<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if (config('app.env') != 'local') {
    URL::forceScheme('https');
}
Route::get('/', function () {
    return view('welcome');
});

Route::get('message', function () {
    $message['user'] = "Juan Perez";
    $message['message'] =  "Prueba mensaje desde Pusher";
    $message['channel'] =  "toma lo tuyo";
    $success = event(new App\Events\NewMessage($message));
    return $success;
});
