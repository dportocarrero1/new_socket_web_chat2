<?php

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('channels.{uid}', function ($user, $uid) {
    return $user->uid == $uid;
});

Broadcast::channel('newMessage.{channelSid}', function ($user, $channelSid) {
    Log::debug('llego a la ruta de chats = ' . $channelSid);
    $channel = \App\Channel::where('channel_id', $channelSid)->first();
    $channel = $channel->toArray();
    $userChannel = $channel[$user->type];
    return  $user->uid == $userChannel;
});


Broadcast::channel('user.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});