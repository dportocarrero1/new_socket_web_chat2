window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo'

window.Pusher = require('pusher-js');

/*window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    // encrypted: true,
    wsHost: window.location.hostname,
    wsPort: 6001
});*/

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    encrypted: true,
    auth: {
        headers: {
            Authorization: 'Bearer ' + "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNmJkMzkyMjBjMTZiZjkxYTJiOGNjYTAzYjIzOGVhYjUwODJiMjkzZGYwMzYxNjAxYzk2NWVmMGEwNWM3ZWQ5MGU2ZThjYjkxNjE0YTA1YjgiLCJpYXQiOjE2MTEwODUwNzksIm5iZiI6MTYxMTA4NTA3OSwiZXhwIjoxNjQyNjIxMDc5LCJzdWIiOiI1Iiwic2NvcGVzIjpbXX0.OEh_IB4CZSair1nGWCDrVK7bbejmVguGkWmMMfdoM38PzZNdYI_B2Nythrt4zR5f9h_uLfRDSBqEIWlPzPRx5ZVLSJYeiymq59aQfQA8zgdK5oqkujxNJRwkFS7twAWdO-UfYEUAETFGOGrIXinu2n_z0NK9BWQnaZfb_VW_iMNiiFa2DHHvdkl_llkse1v6IunAGUuxnaGoVh0fEoCIoCbCnGWbAsaPtJM_H9ZKe1KN0dr3orrhR39rnU28Px9jDyPtt_O2n1SwJNO-vpD6GtDrTXk-foBaHXvwcqHXuGEQbCkTOewXWXaSGW1pwc-NBi6WqyGLXIWydLUEALQ9Nrz7uBekf_gHfFni3X3AlAC9WTsAUM5wKZ-It9B_dCu0J27dDH2mDeQuCLuhlZtgx3AFwi3MUNBxrt3NN8oSF8sAV0RP_iiiFwl9vrskeiB79hSPTroYPPdZDb9NyOD6fvI9reyC9wkHJlaxH2kNChlwDqPbcRMdlZzc7tA6jvPdD8uhCK6S2UBiPDgf0N_QfmdEJaimiTU2oR5qNUyx2iKDdWkHCA-s85fGEgk6eE254jgvHydppbFa2aNDgrG1qVkSMoashJpuyyqTcZVNouA4P5t2rJ2YJ_u0RtvW44QpXxzoTcGpAphxmIroN5pcfjxnl0rWqW3MS6UiL4hUAyw"
        },
    },
});
